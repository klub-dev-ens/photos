# photos

Photo server used on `photos.cof`, using [a fork of Blueimp
Gallery](https://git.eleves.ens.fr/klub-dev-ens/blueimp-gallery.git) for its
frontend.

## Install

```python
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
cp photo/settings.dev.py photo/settings.py  # or .prod.py in production
$EDITOR photo/settings.py  # change settings to appropriate ones
./manage.py migrate
./manage.py loaddata galleryroot_main.yaml  # Create a gallery `main`
```

## Dev setup

In order to get `allauth-ens` working with the SPI's LDAP, you'll have to use a
SSH tunnel, unless you work exclusively from within the ENS — in which case
you'll have to comment out the `CLIPPER_LDAP_SERVER` line in your settings.

```bash
ssh -NL 6363:ldap.spi.ens.fr:636 sas
```
