from django.contrib import admin
from .models import PhotoFile, GalleryRoot


@admin.register(PhotoFile)
class PhotoFileAdmin(admin.ModelAdmin):
    pass


@admin.register(GalleryRoot)
class GalleryRootAdmin(admin.ModelAdmin):
    pass
