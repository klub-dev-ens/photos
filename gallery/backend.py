""" Backend operations

This file handes backend operations at filesystem level such as finding photos in a
directory """

import mimetypes
from pathlib import Path, PurePath

from django.conf import settings
from django.urls import reverse

from .models import PhotoFile, GalleryRoot
from .validators import check_gallery_dir


class GalleryDirectory:
    """ Handles operations on a gallery directory """

    def __init__(self, gallery, path):
        """ If no exception has been raised during __init__, the directory is safe to
        display.
        NOTE: If an exception occurs, the frontend should display the same error in
        every case, to prevent an attacker from discovering the structure of the file
        system """

        check_gallery_dir(path, jail_path=gallery.root_path)
        # ^^^ Aborts early with exception in case of invalid arguments

        self.gallery = gallery
        self.textpath = path
        self.photodir_path = Path(settings.PHOTO_ROOT)
        self.gallery_path = self.photodir_path.joinpath(self.gallery.root_path)
        self.path = self.gallery_path.joinpath(path).resolve(strict=True)
        self.relpath = self.path.relative_to(self.gallery_path)

    def _iterate(self, filter_func=lambda x: True, rel_to=None):
        """ Iterator over the files in this directory, filtering out elements

        Returns a path relative to `rel_to`, or `self.gallery_path` by default """
        if rel_to is None:
            rel_to = self.gallery_path
        for child in self.path.iterdir():
            if filter_func(child):
                yield str(child.relative_to(rel_to))

    def photos(self):
        """ Iterator over the photos in this directory """

        def filter_photo(child):
            if not child.is_file():
                return False
            if child.stem == "_thumbnail":  # Directory's thumbnail, ignored
                return False
            mimetype, _ = mimetypes.guess_type(str(child))
            return mimetype is not None and mimetype.startswith("image/")

        for child in self._iterate(filter_func=filter_photo, rel_to=self.photodir_path):
            print(child)
            photo, _ = PhotoFile.objects.get_or_create_path(child)
            yield photo

    def directories(self):
        """ Iterator over the list of subdirectories """

        def filter_dir(child):
            return child.is_dir()

        for child in self._iterate(filter_func=filter_dir):
            yield GalleryDirectory(self.gallery, child)

    @property
    def name(self):
        """ Name of the directory """
        return self.path.name

    @property
    def url(self):
        """ Url of the directory's gallery view """
        return reverse(
            "gallery:gallerydir",
            kwargs={"cur_path": str(self.relpath), "gallery": self.gallery.name},
        )

    @property
    def thumb(self):
        """ Url of the the directory's thumbnail

        Strategy: use _thumbnail.jpg, else use first image, else use thumbnail of the
        first directory, else fail. """

        def use_photofile(photo):
            return photo.directory_thumbnail_url

        def use_path(path):
            thumb_photo, _ = PhotoFile.objects.get_or_create_path(path)
            return use_photofile(thumb_photo)

        if self.path.joinpath("_thumbnail.jpg").exists():
            return use_path(self.path.joinpath("_thumbnail.jpg"))
        try:
            return use_photofile(next(self.photos()))
        except StopIteration:
            try:
                return use_path(next(self.directories()).thumb)
            except StopIteration:
                return None

    @property
    def parent_path(self):
        parent = self.relpath.parent
        if parent == self.relpath:  # Already at root
            return None
        return parent

    @property
    def parent_url(self):
        parent = self.parent_path
        return GalleryDirectory(self.gallery, parent).url if parent else None
