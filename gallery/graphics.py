""" Graphics operations

This file handles graphical operations, such as generating thumbnails. """

from PIL import Image
from pathlib import Path


def generate_thumbnail(photo_pathstr, thumb_pathstr, dimensions):
    """ Generate a thumbnail for `photo_path` at `thumb_path`, preserving scale and
    fitting in `dimensions`. If a thumbnail at `thumb_path` already exists, does
    nothing. """

    thumb_path = Path(thumb_pathstr)

    if thumb_path.exists():
        return  # Already exists

    thumb_dir = thumb_path.parent
    thumb_dir.mkdir(parents=True, exist_ok=True)

    img = Image.open(photo_pathstr)
    img.thumbnail((dimensions.width, dimensions.height))
    img.save(thumb_pathstr)
