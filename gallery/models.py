import hashlib
import pathlib
import os

from django.conf import settings
from django.db import models

from . import graphics
from .validators import validate_gallery_dir


class BadPhotoPath(Exception):
    """ Raised if the path provided for a PhotoFile is invalid """

    def __init__(self, path):
        self.path = path

    def __str__(self):
        return "Bad photo path: {}".format(self.path)


class PhotoFileManager(models.Manager):
    """ PhotoFile model manager """

    def create_photo(self, path):
        """ Compute sha1sum, check for image duplication, generate thumbnail if needed
        """
        # TODO handle asynchronous generation (AJAX?) to avoid page load time
        photo = self.model(path=path)
        purepath = pathlib.PurePath(path)
        if purepath.is_absolute():
            raise BadPhotoPath(path)

        with open(photo.absolute_path, "rb") as file_handle:  # Might throw errors
            file_bytes = file_handle.read()
            photo.sha1sum = hashlib.sha1(file_bytes).digest()

        graphics.generate_thumbnail(
            photo.absolute_path, photo.thumbnail_path, settings.PHOTO_THUMB_SIZE
        )

        photo.save()

        return photo

    def get_or_create_path(self, path):
        """ Same as `get_or_create`, but operates on `path` and creates using
        `create_photo`. """
        try:
            return self.get(path=path), False
        except self.model.DoesNotExist:
            obj = self.create_photo(path)
            return obj, True


class PhotoFile(models.Model):
    """ An entry referencing a photo on disk

    To create a new photo, please use `PhotoFile.objects.create_photo`. """

    path = models.CharField(
        verbose_name="Photo path",
        help_text="Relative to PHOTO_ROOT",
        unique=True,
        max_length=512,
    )
    sha1sum = models.BinaryField(
        verbose_name="SHA1 hash",
        help_text=(
            "The SHA1 hash of the photo is used to handle thumbnails cleanly, in "
            "particular if a directory is renamed or moved."
        ),
        primary_key=True,
        max_length=20,
    )

    objects = PhotoFileManager()

    def _thumbnail_rel_path(self, size=settings.PHOTO_THUMB_SIZE):
        """ Get this photo's thumbnail path relative to MEDIA_ROOT """
        hex1 = self.sha1sum[:10].hex()
        hex2 = self.sha1sum[10:].hex()
        return os.path.join("thumb", hex1, "{}.{}.jpg".format(hex2, size))

    @property
    def thumbnail_path(self):
        """ Get this photo's thumbnail path on disk """
        return os.path.join(settings.MEDIA_ROOT, self._thumbnail_rel_path())

    @property
    def thumbnail_url(self):
        """ Get this photo's thumbnail URL """
        return os.path.join(settings.MEDIA_URL, self._thumbnail_rel_path())

    @property
    def directory_thumbnail_path(self):
        """ Get this photo's thumbnail path on disk at the directory format

        WARNING: This thumbnail is not automatically generated with this function """
        return os.path.join(
            settings.MEDIA_ROOT,
            self._thumbnail_rel_path(size=settings.DIRECTORY_THUMB_SIZE),
        )

    @property
    def directory_thumbnail_url(self):
        """ Get this photo's thumbnail URL

        WARNING: Calling this function creates the thumbnail if it does not exist """
        if not pathlib.Path(self.directory_thumbnail_path).exists():
            graphics.generate_thumbnail(
                self.absolute_path,
                self.directory_thumbnail_path,
                settings.DIRECTORY_THUMB_SIZE,
            )
        return os.path.join(
            settings.MEDIA_URL,
            self._thumbnail_rel_path(size=settings.DIRECTORY_THUMB_SIZE),
        )

    @property
    def absolute_path(self):
        """ Get this photo's absolute path on disk """
        return os.path.join(settings.PHOTO_ROOT, self.path)

    @property
    def url(self):
        """ Get this photo's URL """
        return os.path.join(settings.PHOTO_URL, self.path)

    @property
    def alt_text(self):
        """ Get this photo's alt text """
        return pathlib.PurePath(self.path).stem


class GalleryRoot(models.Model):
    """ A gallery rooting its arborescence at some subdirectory of `PHOTO_ROOT` """

    name = models.SlugField(
        verbose_name="Gallery name",
        help_text="As it will be in URLs",
        allow_unicode=True,
    )
    root_path = models.CharField(
        verbose_name="Root path",
        help_text="wrt. PHOTO_ROOT",
        max_length=512,
        validators=[validate_gallery_dir],
    )

    def __str__(self):
        return self.name
