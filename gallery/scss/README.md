# Stylesheet using SCSS

The stylesheet for this project is generated using SCSS. To modify it, you can
simply edit the files here, then compile using `make`

## Compiler

This Makefile uses `sassc`, a simple scss compiler. You can however use any
other compiler you wish to use.
