"""photos URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from .views import GalleryDirView

app_name = "gallery"

urlpatterns = [
    path("<slug:gallery>/<path:cur_path>", GalleryDirView.as_view(), name="gallerydir"),
    path(
        "<str:gallery>/",
        GalleryDirView.as_view(),
        name="gallerydir_home",
        kwargs={"cur_path": ""},
    ),
]
