""" Validators for various values """

from pathlib import Path, PurePath
from django.conf import settings
from django.core.exceptions import ValidationError


class InvalidGalleryDir(Exception):
    """ Raised upon bad validation of a gallery_dir """

    def __init__(self, msg, gallery_dir, jail_path):
        super().__init__(self)
        self.msg = msg
        self.gallery_dir = gallery_dir
        self.jail_path = jail_path

    def __str__(self):
        return self.msg


def check_gallery_dir(gallery_dir, jail_path=None):
    """ Check that a string is a valid gallery directory when rooted at `jail_path`

        Raise InvalidGalleryDir upon failure

        * the string mustn't be an absolute path (escape from PHOTO_ROOT),
        * `jail_path` (default: '') is a relative path and must refer, when rooted at
           PHOTO_ROOT, to a subdirectory of PHOTO_ROOT,
        * `os.path.join(jail_path, gallery_dir)` must be an existing directory,
        * this directory must be a subdirectory of `jail_path`
    """

    if jail_path is None:
        jail_path = ""

    photodir_path = Path(settings.PHOTO_ROOT)
    jail_path_rel = PurePath(jail_path)
    if jail_path_rel.is_absolute():
        raise InvalidGalleryDir(
            "Jail path {}: not a relative path".format(jail_path),
            gallery_dir,
            jail_path,
        )
    jail_path = photodir_path.joinpath(jail_path_rel)
    rel_path = PurePath(gallery_dir)
    abs_path = jail_path.joinpath(rel_path)

    if rel_path.is_absolute():
        raise InvalidGalleryDir(
            "{}: not a relative path".format(gallery_dir), gallery_dir, jail_path
        )

    try:
        abs_path = abs_path.resolve(strict=True)
    except FileNotFoundError:
        raise InvalidGalleryDir(
            "{}: no such directory (absolute path: {})".format(gallery_dir, abs_path),
            gallery_dir,
            jail_path,
        )

    # Check that `self.path` is a subdirectory of `jail_path`
    try:
        abs_path.relative_to(jail_path)
    except ValueError:
        raise InvalidGalleryDir(
            "{}: not a subdirectory of {}".format(gallery_dir, jail_path),
            gallery_dir,
            jail_path,
        )

    if not abs_path.is_dir():
        raise InvalidGalleryDir(
            "{}: not a directory".format(gallery_dir), gallery_dir, jail_path
        )


def validate_gallery_dir(gallery_dir, jail_path=None):
    """ Same as `check_gallery_dir`, but raises ValidationError instead """
    try:
        check_gallery_dir(gallery_dir, jail_path)
    except InvalidGalleryDir as exn:
        raise ValidationError(exn.msg)
