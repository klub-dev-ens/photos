from django.conf import settings
from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView
from django.http import Http404

from . import backend
from .backend import GalleryDirectory
from .models import GalleryRoot
from .validators import InvalidGalleryDir


class GalleryDirView(TemplateView):
    """ View listing a directory's content and rendering it as a gallery """

    template_name = "gallery/gallerydir.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        gallery = get_object_or_404(GalleryRoot, name=self.kwargs["gallery"])

        try:
            context["directory"] = GalleryDirectory(gallery, self.kwargs["cur_path"])
        except InvalidGalleryDir as exn:
            if settings.DEBUG:  # Re-raise the same exception if DEBUG
                raise exn
            raise Http404

        context["photos"] = list(context["directory"].photos())
        context["directories"] = list(context["directory"].directories())

        context["nav_up"] = context["directory"].parent_url
        context["cur_directory_name"] = context["directory"].name

        return context
