""" Development settings for Photo

This file IS NOT INTENDED TO BE USED IN PRODUCTION. """

from .settings_base import *
import os

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "v95obld=x5q9m)9dy1)^r*h%#r8rq$1^#67ownqvr@f*&k9z%d"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = "fr-fr"

TIME_ZONE = "Europe/Paris"

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Media path — base directory for media
MEDIA_ROOT = os.path.join(PUBLIC_DIR, "media")

# Photo path — search path for photos on the disk
PHOTO_ROOT = os.path.join(PUBLIC_DIR, "photo")

# Thumbnail settings
PHOTO_THUMB_SIZE = ThumbSize(400, 400)
DIRECTORY_THUMB_SIZE = ThumbSize(200, 200)

# LDAP settings
CLIPPER_LDAP_SERVER = "ldaps://localhost:6363"  # Or anything else
