""" Production settings template for Photo

This file IS NOT INTENDED TO BE USED IN PRODUCTION AS-IS. You should copy this file as
`settings.py`, then address all the parts marked FIXME before using it """

from .settings_base import *
import os

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "CHANGE_ME"  # FIXME

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []  # FIXME

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {  # FIXME
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = "fr-fr"

TIME_ZONE = "Europe/Paris"

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Media path — base directory for media
MEDIA_ROOT = os.path.join(PUBLIC_DIR, "media")

# Photo path — search path for photos on the disk
PHOTO_ROOT = os.path.join(PUBLIC_DIR, "photo")  # FIXME

# Thumbnail settings
PHOTO_THUMB_SIZE = ThumbSize(400, 400)
DIRECTORY_THUMB_SIZE = ThumbSize(200, 200)
